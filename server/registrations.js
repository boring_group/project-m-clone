const mongoose = require('mongoose')
const { Schema } = mongoose

const registrationsSchema = new Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
})

module.exports = mongoose.model('registrations', registrationsSchema)