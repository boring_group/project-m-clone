const express = require('express');
const cors = require('cors'); 
const app = express()
const mongoose = require('mongoose')

app.use(express.json());
app.use(cors()); 

app.listen(9000, () => {
    console.log('on 9000');
})


const registrations = require('./registrations.js')

mongoose.connect('mongodb://127.0.0.1:27017/test')
    .then(() => {
        console.log('Connected to MongoDB');
    })
    .catch((err) => {
        console.error('MongoDB connection error:', err);
    });




app.get('/getCount', async (req, res) => {
    try {
        const resp = await registrations.count()
        res.status(200).json(resp)
    } catch (error) {
        res.status(500).json(error)
    }
})


const emailValidator = require('deep-email-validator')

async function isEmailValid(mail){
    return emailValidator.validate(mail)
}

app.post('/getCount', async (req, res) => {
    try {
        const { email } = req.body;
        const { valid } = await isEmailValid(email)

        if (!email || email.trim() === '') {
            return res.status(200).json('Please enter a valid email address.');
        }

        const existingEmail = await registrations.findOne({ email });

        if (!existingEmail && valid) {
            await registrations.create({ email });
            return res.status(200).json('Pre-registration successful');
        } 
        else if(existingEmail){
            return res.status(200).json('email already exists!');
        }
        else {
            return res.status(200).json('Please enter a valid email address.');
        }
    } catch (error) {
        console.error(error); 
        res.status(500).json('Internal server error');
    }
});

// related to news s
    const file = [
        {
            type: 'news',
            msg: 'Project Mugen PV|Tomorrow and Surprises Come Together！',
            date: '2023-08-24',
            link: 'https://www.projectmugen.com/news/official/en/20230824/39524_1106144.html'
        },
        {
            type: 'events',
            msg: 'PROJECT MUGEN Fly! Over the top of the world', 
            date: '2023-08-24',
            link: 'https://www.projectmugen.com/concept/'
        },
        {
            type: 'news', 
            msg: 'Project Mugen | Disclosure of Production Information', 
            date: '2023-09-21',
            link: 'https://www.youtube.com/watch?v=bmTgg2Tg7GQ'
        },
    ]
    app.get('/news', async function(req, res){
        try {   
            if(file){
                return res.status(200).json(file)
            }
        } catch (error) {
            res.status(500).json({"error": error})
        }
    })

// related to news e

