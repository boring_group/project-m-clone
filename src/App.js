import Naba from './components/NavBar/Naba'
import Header from './components/Header/Header'
import Section from './components/body/Section'
import Section2 from './components/body/Section2'
import Section3 from './components/body/Section3'
import Footer from './components/footer/Footer'
import './index.css'
import store from './components/redux/store'

import { useState } from 'react'

import { Provider } from 'react-redux'


export default function App() {
  



  const [scrollY, setScrollY] = useState()
  const toggleScroll = () => {
    setScrollY(window.scrollY)
}
  window.addEventListener('scroll', toggleScroll)

  return (
   <>
      <Provider store={store}>
        <div >
          <Naba    scrollY={scrollY} />
          <Header  scrollY={scrollY} />
          <Section scrollY={scrollY} />
          <Section2 />
          <Section3 scrollY={scrollY} />
          <Footer />
        </div>
      </Provider>
   </>
  )
}
