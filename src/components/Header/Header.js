import { useEffect, useRef, useState } from 'react';

import { BsFillTriangleFill, BsFillArrowUpLeftCircleFill } from "react-icons/bs";
import './Header.css'

import { useDispatch, useSelector } from 'react-redux';
import { toggleActivate } from '../redux/audioSlice';
import axios from 'axios' 


export default function Header(props) {


  

  const bg1 = useRef()
  const bg2 = useRef()
  const parala1 = useRef()
  const parala2 = useRef()
  const parala3 = useRef()
  const parala4 = useRef()


  const hehe = useRef()
  
  const [cords, setCords] = useState({x:0, y:0})
  const mouseMoveHandler = (e) => {
    const x = e.clientX
    const y = e.clientY
    setCords({x, y})
  }
  
  useEffect(() => {
    hehe.current.addEventListener('mousemove', mouseMoveHandler)
      return () => {
        // eslint-disable-next-line
        hehe.current.removeEventListener('mousemove', mouseMoveHandler)
      }
    // eslint-disable-next-line
  } , [])


  useEffect(() => {
    bg1.current.style.transform = `translate(${cords.x* 0.0002  }%, ${cords.y * 0.0002}%)`
    bg2.current.style.transform = `translate(${cords.x* 0.0005  }%, ${cords.y * 0.0005}%)`

    parala1.current.style.transform = `translate(${ cords.x* 0.006  }%, ${ cords.y * 0.006 }%)`
    parala2.current.style.transform = `translate(${ cords.x* 0.004  }%, ${ cords.y * 0.004 }%)`
    parala3.current.style.transform = `translate(${ cords.x* 0.002  }%, ${ cords.y * 0.002 }%)`
    parala4.current.style.transform = `translate(${ cords.x* 0.001  }%, ${ cords.y * 0.001 }%)`
  },[cords])


  
  useEffect(() => {
    if (props.scrollY !== undefined) {
      if (props.scrollY < 100) {
        for (let i = 0; i < bg2.current.children[1].children.length; i++) {
          bg2.current.children[2].children[i].style.top = '36vi';
        }
      }
      else{
        for (let i = 0; i < bg2.current.children[1].children.length; i++) {
            bg2.current.children[2].children[i].style.top = '28vi';
        }
      }
    }
  }, [props.scrollY])
  

  const dispatch = useDispatch()
  const globSel = useSelector(state => state.audioM)

  const video_pop_up = useRef()
  const triangleClick = () => {
    if (globSel.isAudPlay) {
      dispatch(toggleActivate(!globSel.isAudPlay))
    }
    video_pop_up.current.style.display = 'flex'
    video_pop_up.current.firstChild.firstChild.play()
    
    setTimeout(() => {
      video_pop_up.current.firstChild.classList.add('vid_height_mod')
    }, 10);
  }


  const video_pop_up_closure = (e) => {
    if (e.target === video_pop_up.current) {
      if (globSel.audioIsOn) {
        dispatch(toggleActivate(true))
      }
      video_pop_up.current.style.display = 'none'
      
      video_pop_up.current.firstChild.firstChild.pause()
      video_pop_up.current.firstChild.firstChild.currentTime = 0
      video_pop_up.current.firstChild.classList.remove('vid_height_mod')
    }
  }


  // db connection
  const [count, setCount] = useState()
  useEffect(() => {
    const check = async () => {
      try {
        const data = await axios.get('http://localhost:9000/getCount'); 
        setCount(data.data);
      } catch (e) {
        console.log(e);
      }
    };
    
    check();
  } ,[])
  // db connection


  // related to curved text
    const aroww = useRef()
    const bendArrowEnter = () => {
      if(aroww.current.firstChild){
       aroww.current.firstChild.classList.add('bendedArrowIncrease')
       aroww.current.firstChild.classList.remove('bendedArrow')
      }
    }
    const bendArrowLeave = () => {
      if(aroww.current.firstChild){
        aroww.current.firstChild.classList.add('bendedArrow')
        aroww.current.firstChild.classList.remove('bendedArrowIncrease')
       }
    }
  // related to curved text


  // Related to registering Emails
    const [rMessage, setRMessage] = useState()
    const RegistrationDiv = useRef()
    const registrationClicked = () => {
      RegistrationDiv.current.style.display = 'flex'
    }

    const CloseRegistrationWin = () => {
      RegistrationDiv.current.style.display = 'none'
    } 

    const [loadingCubsStatem, setLoadingCubsState] = useState('none')

    const RegistrationSub = async (e) => {
      e.preventDefault()
      const formData = new FormData(e.target)
      if (formData.get('email').length < 250) {
        try {
          setLoadingCubsState('flex')
          const data = await axios.post('http://localhost:9000/getCount',{email: formData.get('email')})
          setRMessage(data.data.toUpperCase())
        } catch (error) {
          console.log(error);
        }
        setLoadingCubsState('none')
      }
      else{
        setRMessage('EMAIL TOO LONG!')
      }
    }

  // Related to registering Emails



  return (
    <>
        <div className='Header' ref={hehe}>
            <div ref={bg1} className='bg1'></div>

            <div ref={bg2} className='bg2'>
                <BsFillTriangleFill className='triangle' onClick={triangleClick}/>
                <div className='textInsideBG2'>
                    <h3>Global</h3>
                    <h3>Pre-Registration Total</h3>
                    <h1>{count ? count : 'loading...'}</h1>
                    <h1 className='numBg'>{count ? count : 'loading...'}</h1>
                </div>
                <div>
                  <div ref={parala1} className='parala1'></div>
                  <div ref={parala2} className='parala2'></div>
                  <div ref={parala3} className='parala3'></div>
                  <div ref={parala4} className='parala4'></div>
                </div>
            </div>

            <div ref={aroww} className='registrationBtn' onClick={registrationClicked} onMouseEnter={bendArrowEnter} onMouseLeave={bendArrowLeave}>
              <BsFillArrowUpLeftCircleFill className='bendedArrow' />
            </div>

            <div className='RegistrationDiv' ref={RegistrationDiv}>
                <form onSubmit={RegistrationSub}>

                  <span onClick={CloseRegistrationWin}>
                    <span>
                       <div></div>
                       <div></div>
                    </span>
                  </span>

                  <h1>PRE-REGISTER</h1>
                  <h5>Subscribe to the latest Project Mugen updates</h5>
                  <input type='email' name='email' placeholder='Enter your email address'  maxLength={250}/>
                  {rMessage ? <h5>{rMessage}</h5> : ''}
                  <div>
                    <input type='submit' name='submit' value={loadingCubsStatem !== 'flex' ? 'SUBMIT' : ''} disabled={loadingCubsStatem === 'flex'} />

                    <div style={{display: loadingCubsStatem}}>
                      <span></span>
                      <span></span>
                      <span></span>
                    </div>
                  </div>

                </form>
            </div>


          <div className='video-pop-up' ref={video_pop_up} onClick={video_pop_up_closure}>
              <div>
                  <video width="640" height="360" controls disablePictureInPicture={true}>
                    <source src='https://www.projectmugen.com/2023/0823/b4b6908c90566af80af21226c476b8bc.mp4' type="video/mp4"/>
                  </video>
              </div>
          </div>

        </div>
    </>
  )
}
