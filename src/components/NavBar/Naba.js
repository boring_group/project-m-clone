import React from 'react'
import { useRef, useState, useEffect } from 'react'

import { BsHeadphones, BsFillPlayFill } from "react-icons/bs";
import { AiOutlineShareAlt } from "react-icons/ai";
import { motion } from 'framer-motion';

import './Naba.css'

import { useDispatch, useSelector } from 'react-redux';
import { toggleActivate, toggleSwitcher, toggleAudioIsOn } from '../redux/audioSlice';


export default function Naba(props) {

    // nav section
    const u = useRef()
    const firstMenu = useRef()
    const menuBtnCover = useRef()
    const [motionMove, setMotionMove] = useState(0)
    
    const navBtnAct = (e) => {
        const ind = Array.from(u.current.children).indexOf(e.target);

        if (!e.target.classList.contains('active')) {
            Array.from(u.current.children).map(f => {
                return (
                    f.classList.remove('liActivated', 'active'),
                    menuBtnCover.current.addEventListener('animationend', () => {
                        menuBtnCover.current.classList.remove('animateNav');
                    }, { once: true })
                )
            })
        }
        e.target.classList.add('liActivated', 'active')
        menuBtnCover.current.classList.add('animateNav')
        setMotionMove((ind - 1) * 12 + 'vi')

        if(e.target.id === '1'){
            window.scrollTo({
                top: 0,
                behavior: 'auto'
            });

        }
        if(e.target.id === '2'){
            scrollFunk('cardId')
        }
        if(e.target.id === '3'){
            scrollFunk('CardTwo')
        }
        if(e.target.id === '4'){
            scrollFunk('CardThree')
        }
    }

    const scrollFunk = (e) => {
        const elementToScrollTo = document.getElementById(e);
            if (elementToScrollTo) {
                const scrollPosition = elementToScrollTo.offsetTop;
                window.scrollTo({
                    top: scrollPosition,
                    behavior: 'auto'
                });
            }
    }

    const scrollFunkManager = (e, b, c) => {
        const elementToScrollTo  = document.getElementById(e);
        const elementToScrollTo2 = document.getElementById(b);
        const elementToScrollTo3 = document.getElementById(c);
        
        if (elementToScrollTo && u.current) {
            if (parseInt(window.scrollY) < elementToScrollTo.offsetTop - 300) {
                Array.from(u.current.children).forEach((child, index) => {
                    const shouldActivate = index === 1;
                    child.classList.toggle('liActivated', shouldActivate);
                    child.classList.toggle('active', shouldActivate);
                  });
              
                  setMotionMove(`${(1 - 1) * 12}vi`);
            }
            if(parseInt(window.scrollY) >= elementToScrollTo.offsetTop - 300){
                Array.from(u.current.children).forEach((child, index) => {
                    const shouldActivate = index === 2;
                    child.classList.toggle('liActivated', shouldActivate);
                    child.classList.toggle('active', shouldActivate);
                  });
              
                  setMotionMove(`${(2 - 1) * 12}vi`);
            }
            if(parseInt(window.scrollY) >= elementToScrollTo2.offsetTop - 300){
                Array.from(u.current.children).forEach((child, index) => {
                    const shouldActivate = index === 3;
                    child.classList.toggle('liActivated', shouldActivate);
                    child.classList.toggle('active', shouldActivate);
                  });
              
                  setMotionMove(`${(3 - 1) * 12}vi`);
            }
            if(parseInt(window.scrollY) >= elementToScrollTo3.offsetTop - 300){
                Array.from(u.current.children).forEach((child, index) => {
                    const shouldActivate = index === 4;
                    child.classList.toggle('liActivated', shouldActivate);
                    child.classList.toggle('active', shouldActivate);
                  });
              
                  setMotionMove(`${(4 - 1) * 12}vi`);
            }
          
        }
    }

     // scroll tab navBar manager

     useEffect(() => {
        scrollFunkManager('cardId', 'CardTwo', 'CardThree')
        
        // eslint-disable-next-line
      }, [window.scrollY]);
      

     // scroll tab navBar manager
    


    // scroll background manager
    const logo1 = useRef()
    const logo2 = useRef()

    const navBgBk = useRef()

   
    
    useEffect(() => {
        if(logo1 && logo2 && navBgBk && props.scrollY !== undefined){
            if (props.scrollY < 100) {
                // Add an event listener to detect when the logo2 transition ends
                logo2.current.addEventListener('transitionend', () => {
                  navBgBk.current.style.top = '0vi';
                  logo2.current.style.transform = 'scale(1) translateY(0vi)';
                  logo1.current.style.opacity = '0';
                });
            
                logo2.current.style.opacity = '1';
              }
              else {
                // Add an event listener to detect when the logo2 transition ends
                logo2.current.addEventListener('transitionend', () => {
                  navBgBk.current.style.top = '-15vi';
                  logo2.current.style.transform = 'scale(0) translateY(-10vi)';
                  logo1.current.style.opacity = '1';
                });
            
                logo2.current.style.opacity = '0';
              }
        }
    }, [props.scrollY])

    // scroll background manager

    
    // aud

    const dispatch = useDispatch()
    const globSel = useSelector(state => state.audioM)

    const body = document.getElementsByTagName('body')[0]
    const aud = useRef()

    const handleBodyClick = () => {
        if (!globSel.switcher) {
            dispatch(toggleSwitcher(true));
            dispatch(toggleAudioIsOn(true))
            aud.current.play();
        }
    };
    useEffect(() => {
        body.addEventListener('click', handleBodyClick)

        return () => {
            body.removeEventListener('click', handleBodyClick);
        };
    // eslint-disable-next-line
    }, [globSel.switcher, body]);

    useEffect(() => {
        if (globSel.switcher) {
            if (!globSel.isAudPlay) {
                aud.current.pause();
            }
            else{
                aud.current.play();
            }
        }  
    } ,[globSel.isAudPlay, globSel.switcher])
    

    const toggleAud = () => {
        if (globSel.switcher) {
            if (globSel.isAudPlay) {
                dispatch(toggleActivate(!globSel.isAudPlay))
                dispatch(toggleAudioIsOn(false))
            }
            else{
                dispatch(toggleActivate(!globSel.isAudPlay))
                dispatch(toggleAudioIsOn(true))
            }
        }  
    }

    const sendToOr = () => {
        window.open('https://www.projectmugen.com/index.html', '_blank')
    }

    // aud

    // nav section

  return (
    <div className='navBarBigDiv'>
        <div className='navBgBk' ref={navBgBk}></div>
        <nav >
            <div className='logoHolder' >
                <img ref={logo1} className='logo1' src='https://www.projectmugen.com/pc/gw/20230811194459/assets/logo-big_5c93dc39.png' alt='logo'/>
                <img ref={logo2} className='logo2' src='https://www.projectmugen.com/pc/gw/20230811194459/assets/logo-big_5c93dc39.png' alt='logo'/>
            </div>
            <div>
                <ul ref={u}>
                    <motion.div  ref={menuBtnCover} style={{left: motionMove}} className='menuBtnCover'></motion.div>
                    <li ref={firstMenu} id='1' className='liActivated active' onClick={navBtnAct}>MAIN</li>
                    <li id='2' onClick={navBtnAct}>CHARACTERS</li>
                    <li id='3' onClick={navBtnAct}>NEW & INFO</li>
                    <li id='4' onClick={navBtnAct}>FEATURES</li>
                </ul>
            
                <div>
                    <button className='menuBTN' onClick={toggleAud}>{globSel.isAudPlay ? <BsHeadphones /> : <BsFillPlayFill />}
                        <audio ref={aud} src='https://www.projectmugen.com/2023/0823/602b7fc74a07ea2ca43d454626a6c181.mp3' loop></audio>
                    </button>
                    <button className='menuBTN' onClick={sendToOr}><AiOutlineShareAlt /></button>
                </div>
            </div>
        </nav>
    </div>
  )
}
