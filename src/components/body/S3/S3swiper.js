import React, { useEffect, useRef, useState } from 'react';
import { FaArrowLeft, FaArrowRight } from 'react-icons/fa';


import { Autoplay, Scrollbar, A11y, Pagination } from 'swiper/modules';
import { Swiper, SwiperSlide } from 'swiper/react';

import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import 'swiper/css/scrollbar';
import './S3swiper.css';

export default function S3swiper({ slides }) {

  const [swiperInstance, setSwiperInstance] = useState(null);
  const onSwiperInit = (swiper) => {
    setSwiperInstance(swiper);
  };


  const swiperRef = useRef()

  const observer = new IntersectionObserver((entries) => {
    entries.forEach(entry => {
      if (entry.target === swiperRef.current) {
        entry.target.classList.toggle('swiperRef__active', entry.isIntersecting)
      }
    })
  },{threshold: 1,})

  useEffect(() => {
    observer.observe(swiperRef.current)
    // observer.observe(swiperRef.current)
    // eslint-disable-next-line
  }, [swiperRef])


  return (
    <>
        <div className='dev_dev_swiper' ref={swiperRef}> 
          <button onClick={() => swiperInstance.slidePrev()}><FaArrowLeft /></button>
          <div className='dev_swiper'>
            <Swiper
              modules={[Autoplay, Scrollbar, A11y, Pagination]}
              loop={true}
              allowTouchMove={false}
              autoplay={{ delay: 4000 }}
              spaceBetween={0}
              slidesPerView={1}
              pagination={{ el: ".my_custom_pag",clickable: true }}
              onSwiper={onSwiperInit}
            >
              {slides.map((slide) => (
                <SwiperSlide key={slide.id}>
                  <img className='sliderIMg' src={slide.image} alt={slide.title} />
                </SwiperSlide>
              ))}
            </Swiper>
            <div className='my_custom_pag'></div>
          </div>
          <button onClick={() => swiperInstance.slideNext()}><FaArrowRight /></button>

      </div>

    </>
  );
}
