import { useEffect, useRef, useState } from 'react'
import './Section.css'

import { BsFillCaretLeftFill, BsFillCaretRightFill } from 'react-icons/bs'

export default function Section (props) {
    
    const cardid = useRef()
    const numberIdentifier = useRef()
    const Strips = useRef()
    const Strips2 = useRef()
    const Strips3 = useRef()
    const cardSlider = useRef()


    useEffect(() => {
        const cal =  parseInt(props.scrollY) * -1
        Strips.current.children[0].style.transform  = `translateX(${cal}px)`;
        Strips2.current.children[0].style.transform = `translateX(${cal}px)`;
        Strips3.current.children[0].style.transform = `translateX(${cal}px)`;
        
    } ,[props.scrollY])

    const observer = new IntersectionObserver(entries => {
        entries.forEach(entry => {
            if (entry.target ===  numberIdentifier.current) {
                entry.target.classList.toggle('numberIdentifier__active', entry.isIntersecting)
                setTimeout(() => {
                    numberIdentifier.current.lastChild.style.right = '0'
                },100)
            }
            if (entry.target === cardSlider.current) {
                entry.target.classList.toggle('cardActivator', entry.isIntersecting)
            }
        })
    },{
        threshold: 0,
    })

    useEffect(() => {
            observer.observe(numberIdentifier.current)
            observer.observe(cardSlider.current)
    // eslint-disable-next-line
    },[numberIdentifier, cardSlider])

    

    // slider Related 
        const [sliderName, setSliderName] = useState('Taffy')
        const slider = useRef()
        const [isTransitioning, setIsTransitioning] = useState(false);
        const carousel = useRef()



        // images id:
            const cardImages = useRef()
            const cardSliderImages = useRef()


        // Slider Name Related

        const toTheRight = () => {
            if (slider !== undefined && !isTransitioning) {
                setSliderName(slider.current.children[2].getAttribute('name'));
                cardsChecker(slider.current.children[2].getAttribute('name'));
                slider.current.style.transform = 'translate(-220%)' 
                slider.current.addEventListener('transitionend', grampy, { once: true })
            }
        }

        const toTheLeft = () => {
            if (slider !== undefined && !isTransitioning) {
                    setSliderName(slider.current.children[0].getAttribute('name'));
                    cardsChecker(slider.current.children[0].getAttribute('name'));
                    setIsTransitioning(true)
                    slider.current.style.transform = 'translate(0%)' 
                    slider.current.addEventListener('transitionend', granny, { once: true })
            }
        }
        // Slider Name Related
        
        // right
        const grampy = () => {
            slider.current.appendChild(slider.current.firstElementChild)
            slider.current.style.transition = 'none'
            slider.current.style.transform = 'translate(-110%)' 
            setTimeout(() => {
                slider.current.style.transition = '0.25s ease-in-out transform'
                setIsTransitioning(false)
            });
        }
        
        // left
        const granny = () => {
            const firstChild = slider.current.firstElementChild;
            const lastChild = slider.current.lastElementChild;
            slider.current.insertBefore(lastChild, firstChild);
            slider.current.style.transition = 'none'
            slider.current.style.transform = 'translate(-110%)' 
            setTimeout(() => {
                slider.current.style.transition = '0.25s ease-in-out transform'
                setIsTransitioning(false)
            });
        }

        // double Right
        const dumpy = () => {
            slider.current.appendChild(slider.current.firstElementChild)
            slider.current.style.transition = 'none'
            slider.current.style.transform = 'translate(-110%)' 
            setTimeout(() => {
                slider.current.style.transition = '0.25s ease-in-out transform'
                setIsTransitioning(false)
            });
        }
        
        // shortCut so I won't repeat my self
        const cardsChecker = (e) => {
            for (let i = 0; i < cardImages.current.children.length; i++) {
                if (cardImages.current.children[i].name !== e) {
                    cardSliderImages.current.children[i].classList.remove('cardSliderImgActivator')
                    cardImages.current.children[i].classList.remove('cards_in')
                    cardImages.current.children[i].classList.add('cards_out')
                }
                else{
                    cardSliderImages.current.children[i].classList.add('cardSliderImgActivator')
                    cardImages.current.children[i].classList.add('cards_in')
                    cardImages.current.children[i].classList.remove('cards_out')
                }
            }
        }
        
        // Slider Name Related
        const sliderDiv = (e) => {
            slider.current.addEventListener('click', (e) => {
                if (slider !== undefined && !isTransitioning ) {
                    setSliderName(e.target.getAttribute('name'));
                    cardsChecker(e.target.getAttribute('name'));

                    if (e.target === slider.current.children[2]) {
                        slider.current.style.transform = 'translate(-220%)';
                        slider.current.addEventListener('transitionend', grampy, { once: true });
                    } else if (e.target === slider.current.children[3]) {
                        setTimeout(() => {
                            slider.current.appendChild(slider.current.firstElementChild)
                        }, 100);
                        slider.current.style.transform = 'translate(-220%)';
                        slider.current.addEventListener('transitionend', dumpy, { once: true })
                    }
                }
            }, {once: true});
        };
        // Slider Name Related

    // slider Related
    



    return(

        <>
        {/* <div className="mainSection"></div> */}

        <div  className='cardOne'>
            <div id='cardId' ref={cardid}>
                <div className='numberIdentifier' ref={numberIdentifier}>
                    <h1>01</h1>
                    <p>CHARACTERS</p>
                </div>
                <div ref={cardImages}>
                    <img className='role_cards cards_out' name={'Dila'}       src='https://www.projectmugen.com/pc/gw/20230811194459/assets/bg4_add4edb0.png' alt='anImg'/>
                    <img className='role_cards cards_in'  name={'Taffy'}      src='https://www.projectmugen.com/pc/gw/20230811194459/assets/bg5_8042603f.png' alt='anImg'/>
                    <img className='role_cards cards_out' name={'Bansy'}      src='https://www.projectmugen.com/pc/gw/20230811194459/assets/bg1_e0744813.png' alt='anImg'/>
                    <img className='role_cards cards_out' name={'Alan'}       src='https://www.projectmugen.com/pc/gw/20230811194459/assets/bg2_609cb7d7.png' alt='anImg'/>
                    <img className='role_cards cards_out' name={'Mechanika'}  src='https://www.projectmugen.com/pc/gw/20230811194459/assets/bg3_b43a0a4a.png' alt='anImg'/>
                </div>
            </div>

            <div className='cardSliderGrandDiv'>
                <div className='cardSlider' ref={cardSlider}>
                    <div ref={cardSliderImages}>
                        <img src='https://www.projectmugen.com/pc/gw/20230811194459/assets/role-4_8e1d15fb.png' name={'Dila'}       alt='anImg'/>
                        <img className='cardSliderImgActivator' src='https://www.projectmugen.com/pc/gw/20230811194459/assets/role-5_55e3b25a.png' name={'Taffy'}      alt='anImg'/>
                        <img src='https://www.projectmugen.com/pc/gw/20230811194459/assets/role-1_c7a01cc3.png' name={'Bansy'}      alt='anImg'/>
                        <img src='https://www.projectmugen.com/pc/gw/20230811194459/assets/role-2_3501da78.png' name={'Alan'}       alt='anImg'/>
                        <img src='https://www.projectmugen.com/pc/gw/20230811194459/assets/role-3_7bb124dd.png' name={'Mechanika'}  alt='anImg'/>
                    </div>
                </div>
                <div className='cardSliderController'>
                    <h1>{sliderName}</h1>
                    <div className='container'>
                        <button onClick={toTheLeft}><BsFillCaretLeftFill className='arrows'/></button>
                        <div className='carousel' ref={carousel}>
                            <div className='slider' ref={slider} >
                                <div onClick={sliderDiv} name={'Dila'}>
                                    <img id='slider5' src='https://www.projectmugen.com/pc/gw/20230811194459/assets/review-4_40461b59.png' alt='slider5'/>
                                </div>
                                <div onClick={sliderDiv} name={'Taffy'}>
                                    <img id='slider1' src='https://www.projectmugen.com/pc/gw/20230811194459/assets/review-5_cc74eaf2.png' alt='slider1'/>
                                </div>
                                <div onClick={sliderDiv} name={'Bansy'}>
                                    <img id='slider2' src='https://www.projectmugen.com/pc/gw/20230811194459/assets/review-1_4e0d2a5a.png' alt='slider2'/>
                                </div>
                                <div onClick={sliderDiv} name={'Alan'}>
                                    <img id='slider3' src='https://www.projectmugen.com/pc/gw/20230811194459/assets/review-2_4abec490.png' alt='slider3'/>
                                </div>
                                <div onClick={sliderDiv} name={'Mechanika'}>
                                    <img id='slider4' src='https://www.projectmugen.com/pc/gw/20230811194459/assets/review-3_3f5b4e9f.png' alt='slider4'/>
                                </div>
                            </div>
                        </div>
                        <button onClick={toTheRight}><BsFillCaretRightFill className='arrows'/></button>
                    </div>
                </div>
            </div>

        </div>

        <div className='stripsController'>
            <div ref={Strips} className='Strips'>
                <div></div>
            </div>
            <div ref={Strips2} className='Strips2'>
                <div></div>
            </div>
            <div ref={Strips3} className='Strips3'>
                <div></div>
            </div>
        </div>
        
        </>

    )

}