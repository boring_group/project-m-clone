import { useRef, useEffect, useState } from 'react'
import axios from 'axios'
import './Section2.css'

export default function Section2(){

    const numberIdentifier2 = useRef()
    const news_box_Div = useRef()
    const news_box_list = useRef()
    const news_ra_list = useRef()
    const cardid2 = useRef()
    

    const observer = new IntersectionObserver(entries => {
        entries.forEach(entry => {
            if (entry.target === numberIdentifier2.current) {
                entry.target.classList.toggle('numberIdentifier2__active', entry.isIntersecting)
                setTimeout(() => {
                    numberIdentifier2.current.lastChild.style.right = '0'
                },100)
            }
            if (entry.target === news_box_Div.current) {
                entry.target.classList.toggle('news-box-Div__active', entry.isIntersecting)
            }
        })
    }, {
        threshold: 0,
    })

    useEffect(() => {

            observer.observe(numberIdentifier2.current)
            observer.observe(news_box_Div.current)
        // eslint-disable-next-line
        }, [numberIdentifier2])

    // related to swiper

    let active = useRef(0);
    let swipers = useRef(0);
    let refresh1 = useRef(null);
    
    useEffect(() => {
        if (news_box_list.current !== undefined) {
            swipers.current = news_box_list.current.children.length - 1;
        }
    }, [news_box_list]);

    const reloadSlider = () => {
        if (news_box_list.current && news_ra_list.current) {
            news_box_list.current.style.left = 50 + (-100 * active.current) + '%';
    
            if (news_ra_list.current.children) {
                for (let i = 0; i < news_ra_list.current.children.length; i++) {
                    if (news_ra_list.current.children[i]) {
                        news_ra_list.current.children[i].classList.remove('activated');
                    }
                }
            }
    
            if (news_ra_list.current.children[active.current]) {
                news_ra_list.current.children[active.current].classList.add('activated');
            }
        }
    
        clearInterval(refresh1.current);
        refresh1.current = setInterval(() => {
            if (active.current + 1 > swipers.current) {
                active.current = 0;
            } else {
                active.current += 1;
            }
            reloadSlider();
        }, 5000);
    };

    const radioClicked = (e) => {
        active.current = e;
        reloadSlider();
    };

    useEffect(() => {
             refresh1.current = setInterval(() => {
                if (active.current + 1 > swipers.current) {
                    active.current = 0;
                } else {
                    active.current += 1; 
                }
                reloadSlider();
            }, 5000);

        
        return () => clearInterval(refresh1.current);
        // eslint-disable-next-line
    }, []);


    let net = 'http://localhost:9000'
    const [data, setData] = useState(null)
    const [dataFilterer, setDataFilterer] = useState(null)
    useEffect(() => {
        const check = async () => {
            try {
                const da = await axios.get(net+'/news')
                da.data.sort((a, b) => {
                    let ad = new Date(a.date)
                    let bd = new Date(b.date)
                    return  bd - ad
                })
                setDataFilterer([...da.data])
                setData([...da.data])
            } catch (error) {
                console.log(error);
            }
        }
        check()
    }, [net])

    const linkCLickedFromData = (e) => {
        window.open(e)
    }


   const links = document.querySelectorAll('.lilis')

    const latest = (e) => {
        setDataFilterer(data.filter((f) => f))
        if(links !== undefined){
            // eslint-disable-next-line
            switch (e.target.getAttribute('value')) {
             case 'LATEST':
                setDataFilterer(data.filter((f) => f))
                links.forEach(link => link.classList.remove('liActivator'))
                links[0].classList.add('liActivator')
                    break;
            
             case 'NEWS':
                setDataFilterer(data.filter((f) => f.type === 'news'))
                links.forEach(link => link.classList.remove('liActivator'))
                links[1].classList.add('liActivator')
                    break;

             case 'EVENTS':
                setDataFilterer(data.filter((f) => f.type === 'events'))
                links.forEach(link => link.classList.remove('liActivator'))
                links[2].classList.add('liActivator')
                    break;

             case '+':
                window.open('https://www.projectmugen.com/news/en/')
                    break;
        }}
        
    }


    // related to swiper


    return(
        <>
            <div id="CardTwo" ref={cardid2}>

                <div className='cardId2' >
                    <div className='numberIdentifier2' ref={numberIdentifier2}>
                        <h1>02</h1>
                        <p>NEWS & INFO</p>
                    </div>
                </div>

                <div ref={news_box_Div} className='news-box-Div'>
                    <div className='news-box' >
                        <div ref={news_box_list}>
                            <a href='https://www.youtube.com/watch?v=bmTgg2Tg7GQ' target='_blank' rel="noreferrer" title='Project Mugen | Disclosure of Production Information'>
                                <img src='https://r.res.easebar.com/pic/20230921/3cc14978-35e5-469c-bd7c-49a9cf6b6a73.jpg' alt='anImg'/>
                                <p>Project Mugen | Disclosure of Production Information</p>
                            </a>
                            <a href='https://www.projectmugen.com/concept/' target='_blank' rel="noreferrer" title='PROJECT MUGEN Fly! Over the top of the world'>
                                <img src='https://r.res.easebar.com/pic/20230823/ababe5e2-abe6-40f8-86e5-dbd702698301.jpg' alt='anImg'/>
                                <p>PROJECT MUGEN Fly! Over the top of the world</p>
                            </a>
                        </div>
                        <ul ref={news_ra_list}>
                                {news_box_list.current !== undefined
                                  ? Array.from(news_box_list.current.children).map((e, i) => {
                                      return <li className={i === 0 ? 'activated' : ''} key={i} onClick={() => {radioClicked(i)}}></li>;
                                    })
                                  : null
                                }
                        </ul>
                        <div className='news-box-news'>
                                <div>
                                    <ul>
                                        <li className='lilis liActivator' onClick={latest} value={'LATEST'}>LATEST</li>
                                        <li className='lilis' onClick={latest} value={'NEWS'}>NEWS</li>
                                        <li className='lilis' onClick={latest} value={'EVENTS'}>EVENTS</li>
                                        <li className='lilis' onClick={latest} value={'+'}>+</li>
                                    </ul>
                                </div>
                                <div>
                                {dataFilterer !== null && dataFilterer.map((e, i) => (
                                    <>
                                    <div key={i} onClick={() => {linkCLickedFromData(e.link)}}>
                                        <span>{e.type.toUpperCase()}</span>
                                        <span>{e.msg.length > 55 ? e.msg.slice(0,55) + '...' : e.msg}</span>
                                        <span>{e.date}</span>
                                    </div>
                                    </>
                                ))}
                                </div>
                        </div>
                    </div>
                </div>

            </div>
        </>
    )
}