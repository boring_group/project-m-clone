import { useEffect, useRef } from 'react'
import S3swiper from './S3/S3swiper'
import S3slides from './S3/S3slides.json'
import './Section3.css'




export default function Section3() {


  const numberIdentifier3 = useRef()

  const observer = new IntersectionObserver((entries) => {
    entries.forEach(entry => {
      if (entry.target === numberIdentifier3.current) {
          entry.target.classList.toggle('numberIdentifier3__active', entry.isIntersecting)
          setTimeout(() => {
            numberIdentifier3.current.lastChild.style.right = '0'
          },100)
      }
    })
  },{threshold: 1,})

  useEffect(() => {
    observer.observe(numberIdentifier3.current)
    // observer.observe(swiperRef.current)
    // eslint-disable-next-line
  }, [numberIdentifier3])

  return (
    <>
        <div id="CardThree">
            <div className='cardId3' >
                <div className='numberIdentifier3' ref={numberIdentifier3}>
                    <h1>03</h1>
                    <p>FEATURES</p>
                </div>
            </div>

           <S3swiper slides={S3slides} />
        </div>
    </>
  )
}
