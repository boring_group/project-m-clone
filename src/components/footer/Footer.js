import React from 'react'
import './Footer.css'

export default function Footer() {
  return (
    <div className='Footer'>
        <div>
            <img src='https://www.projectmugen.com/pc/gw/20230811194459/assets/logo-w_9e210eb9.png' alt='logo'/>
            <div>
                <p>©1997-2023 NetEase, Inc.All Rights Reserved</p>
                <p><a href="https://protocol.unisdk.easebar.com/release/latest_v475.html" target="_blank" rel="noreferrer">Privacy Policy.</a> and <a href="https://protocol.unisdk.easebar.com/release/latest_v487.html" target="_blank" rel="noreferrer">User Agreement</a>, Terms and Conditions</p>
                <p>Company Address：128 Beach Road, #19-01 Guoco MidTown, Singapore 189773</p>
                <p>Connection for Business：global@projectmugen.com</p>
            </div>
        </div>
    </div>
  )
}
