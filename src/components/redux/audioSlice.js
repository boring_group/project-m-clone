import { createSlice } from "@reduxjs/toolkit";

const audioSlice = createSlice({
    name: 'audioM',
    initialState: { isAudPlay: true, switcher: false, audioIsOn: false },
    reducers: {
        toggleActivate: (state, action) => {
            state.isAudPlay = action.payload;
        },
        toggleSwitcher: (state, action) => {
            state.switcher = action.payload;
        },
        toggleAudioIsOn: (state, action) => {
            state.audioIsOn = action.payload;
        }
    }
});

export const { toggleActivate, toggleSwitcher,toggleAudioIsOn } = audioSlice.actions;
export default audioSlice.reducer;
