import { configureStore } from "@reduxjs/toolkit";
import audioReducer from "./audioSlice";

const store = configureStore({ reducer: { audioM: audioReducer } });

export default store;
