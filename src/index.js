import reactDOM from 'react-dom/client'
import App from './App'

const ele = document.getElementById('root')
const root = reactDOM.createRoot(ele)

root.render(<App />)